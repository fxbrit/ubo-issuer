import requests
import gitlab
import os
from packaging import version

def main():
    project_id = 13852721 # project id of the settings repo of librewolf
    token = "" # insert your personal access token here
    label = "update::uBO" # label specific for issues created by this script

    release = requests.get("https://api.github.com/repos/gorhill/uBlock/releases", json={"key": "value"})
    policy = requests.get("https://gitlab.com/librewolf-community/settings/-/raw/master/distribution/policies.json", json={"key": "value"})
    last_issue = requests.get("https://gitlab.com/api/v4/projects/%s/issues?labels=%s" %(project_id, label))

    if policy.status_code == 200 :
        current = policy.json()["policies"]["Extensions"]["Install"][0].split("ublock_origin-", 1)[1].split("-")[0] # uBO version in the policies
    
    if last_issue.status_code == 200 :
        try :
            last_issue_version = last_issue.json()[0]["title"].split("Origin to ")[1] # uBO version from the last opened issue
        except:
            last_issue_version = "0.0.0" # if no issue exists with the default syntax, always create a new one

    if release.status_code == 200 :
        j = release.json()
        for x in range(0, len(j)) :
            ver = j[x]["tag_name"] 
            '''
            'tag_name' contains the version in the format x.y.z, if z contains 'b' then it is beta, if it contains 'rc' then it's release candidate.
            beta and rc versions are not released, skip them. take first non-beta and non-rc version as it is a release.

            then check if latest version is newer than one in policies, and than one in last opened issue of this kind.
            this avoids opening issues on every new check, unless strictly needed. checks for both closed and open issues for peace of mind.
            '''
            if ver.split(".")[2].find("b") == -1 and ver.split(".")[2].find("rc") == -1 and version.parse(ver) > version.parse(current) and version.parse(ver) > version.parse(last_issue_version) :
                title = "Update uBlock Origin to " + ver
                body = "uBO version " + ver + " has been released on github, so it will be soon on the addons website. update `policies.json`."
                url="https://gitlab.com/api/v4/projects/%s/issues?title=%s&description=%s&labels=%s" %(project_id, title, body, label)
                response = requests.post(url, headers={"PRIVATE-TOKEN": token})
                print(response.status_code)
                break

main()
